# recipe-app-api-proxy

Recipe app API proxy application

NGINX proxy app for our recipe app API

## USAGE


### Env Variable

* `LISTEN_PORT` - Port to listen on (defaults: `8000`)
* `APP_HOST` - Hostname of the app to forward requests to (default: `app`)
* `APP_PORT` - Port of the app to forward to (default: `9000`)
